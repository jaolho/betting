﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;

namespace ConsoleApplication1
{
  class State {
    public State(double odds, double voteCount, String name) {
      this.odds = odds;
      this.voteCount = voteCount;
      this.name = name;
    }

    public State() { }

    public double odds = 0;
    public double voteCount = 0;
    public String name = "";
  }

  class StateCombo {
    public StateCombo(List<State> list, List<State> fullList)
    {
      this.list = new List<State>(list);
      CombineValues(fullList);
    }
    public List<State> list = new List<State>();
    public double odds;
    public double voteCount;
    public void CombineValues(List<State> fullList)
    {
      odds = fullList.ConvertAll(s => list.Contains(s) ? s.odds : 1 - s.odds).Aggregate((a, x) => a * x);
      voteCount = list.Sum(s => s.voteCount);
    }
    public String toString()
    {
      return voteCount.ToString("f0") + ", " + odds.ToString("f4") + ", " + String.Join(", ", list.ConvertAll(s => s.name).ToArray());
    }
    public void Print()
    {
      Console.WriteLine(toString());
    }
  }

  class StateOdds
  {
    List<State> allStates;
    List<StateCombo> stateComboList;
    double zeroLimit = 5;
    int sureVotes = 180;

    public StateOdds(String filename)
    {
      Console.WriteLine("File: " + filename);
      ParseData(filename);
      /*
        76.75
        64.25
        51.75
        39.25
        26.75
        14.25
       */
      CreateStateCombos();
      PrintResult(77);
      PrintResult(65);
      PrintResult(52);
      PrintResult(40);
      PrintResult(27);
      PrintResult(15);
    }

    void ParseData(String filename) {
      allStates = new List<State>();
      File.ReadAllLines(filename).ToList().ForEach(s =>
      {
        State state = new State();
        // state name
        state.name = s.Split('(')[0].Replace(" ", ""); 
        // vote count
        state.voteCount = int.Parse(s.Split('(', ')')[1]);
        // poll percentage
        String r2 = s.Split(')')[1];
        if (r2.Contains("Trump"))
          state.odds = double.Parse(r2.Replace("Trump", "").Replace(" ", ""));
        else if (r2.Contains("Clinton"))
          state.odds = double.Parse(r2.Replace("Clinton", "").Replace(" ", "").Replace("+", "-"));
        else
          state.odds = 0;
        state.odds = PollToOdds(state.odds);
        allStates.Add(state);
      });
    }

    double PollToOdds(double poll) {
      return (poll < -zeroLimit) ? 0 : ((poll > zeroLimit) ? 1 : (poll + zeroLimit) / (2 * zeroLimit));
    }

    void CreateStateCombos() {
      List<List<State>> combos = GetAllCombos(allStates);
      stateComboList = new List<StateCombo>();
      combos.ForEach(c => stateComboList.Add(new StateCombo(c, allStates)));
    }

    void PrintResult(int voteLimit)
    {
      List<StateCombo> winningCombos = stateComboList.FindAll(sc => sc.voteCount >= voteLimit);
      double P_AnyWinningCombo = winningCombos.Sum(sc => sc.odds);
      Console.WriteLine("Trump votes at least: " + (sureVotes + voteLimit) + ", chance: " + P_AnyWinningCombo.ToString("f2") + ", betting odds: " + (1 / P_AnyWinningCombo).ToString("f2"));
    }

    public static List<List<T>> GetAllCombos2<T>(List<T> list)
    {
      List<List<T>> result = new List<List<T>>();
      // head
      result.Add(new List<T>());
      result.Last().Add(list[0]);
      if (list.Count == 1)
        return result;
      // tail
      List<List<T>> tailCombos = GetAllCombos2(list.Skip(1).ToList());
      tailCombos.ForEach(combo =>
      {
        result.Add(new List<T>(combo));
        combo.Add(list[0]);
        result.Add(new List<T>(combo));
      });
      return result;
    }

    public static List<List<T>> GetAllCombos<T>(List<T> list)
    {
      int comboCount = (int)Math.Pow(2, list.Count) - 1;
      List<List<T>> result = new List<List<T>>();
      for (int i = 1; i < comboCount + 1; i++)
      {
        // make each combo here
        result.Add(new List<T>());
        for (int j = 0; j < list.Count; j++)
        {
          if ((i >> j) % 2 != 0)
            result.Last().Add(list[j]);
        }
      }
      return result;
    }

    static void Main(string[] args)
    {
      //String[] s1 = Directory.GetFiles(".");
      Directory.GetFiles(".").ToList().FindAll(s => s.Contains("tossups")).ForEach(s => new StateOdds(s));  
    }
  }
}
